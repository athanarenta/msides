@extends('layouts.main')

@section('title','MMSide')

@section('content')
<section>
    <div class="container py-5">
      <form class="row mb-5">

        <div class="col-sm-6 col-md-4 col-lg-3 mb-4">
          <div class="select-wrap">
            <input type="text" id="luas" class="form-control" placeholder="Luas Tanah">
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3 mb-4">
          <div class="select-wrap">
            <span class="icon icon-arrow_drop_down"></span>
            <select name="offer-types" id="tipe" class="form-control d-block rounded-0">
              <option harga="0" value="">Satuan</option>
              <option harga="37800000" value="ubin">Ubin</option>
              <option harga="2700000" value="m">M<sup>2</sup></option>
              <option harga="250000" value="m">Kanopi</option>
            </select>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3 mb-4">
          <div class="select-wrap">
            <input type="text" id="hasil" class="form-control" readonly="readonly" placeholder="Hasil">
          </div>
        </div>

        <div class="col-sm-6 col-md-4 col-lg-3 mb-4">
          <input type="submit" class="btn btn-primary btn-block form-control-same-height rounded-0" value="Search">
        </div>
      </form>
      <div class="">
        <div class="row">
            <div class="cold-md-6 px-3">
                <div class="w-100">Cal</div>
            </div>
            <div class="cold-md-6 px-3">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama Pekerjaan</th>
                                <th scope="col">Pengerjaan (Tanggal)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>SatuSatuSatuSatuSatuSatuSatuSatu SatuSatu SatuSatu SatuSatuSatuSatuSatuSatu SatuSatu</td>
                                <td>Dua</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
      </div>


    </div>
</section>
<section class="min-vh-100 bg-dark mb-5">
    <div class="container" style="padding-top:10%">
        <div class="row">
            <div class="col-md-6 px-5">
                <div class="card">
                    <div class="card-body bg-light rounded-lg px-5">
                        <h4 class="font-weight-bold text-center">Jasa Layanan Konstruksi</h4>
                        <hr width="50%" color="black">
                        <p class="small">MM side menawarkan segala kebutuhan yang jujur dan amanah dengan biaya yang sangat terjangkau dan berkualitas tinggi.</p>
                        <div class="float-right">
                            <button class="btn btn-primary font-weight-light px-4">Pilih</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 px-5 my-auto">
                <div class="card">
                    <div class="card-body bg-light rounded-lg px-5">
                        <h4 class="font-weight-bold text-center">Galeri Properti</h4>
                        <hr width="50%" color="black">
                        <p class="small">MM Side menjual properti dari seluruh masyarakat Indonesia mulai dari rumah, tanah, hingga perabot dengan kualitas produk 100% sesuai deskripsi. Jual properti anda disini sekarang juga.</p>
                        <div class="float-right">
                            <button class="btn btn-primary font-weight-light px-4">Pilih</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 px-5 my-5">
                <div class="card">
                    <div class="card-body bg-light rounded-lg px-5">
                        <h4 class="font-weight-bold text-center">Pembuatan Furnitur</h4>
                        <hr width="50%" color="black">
                        <p class="small">MM Side melayani permintaan pembuatan furniture sesuai dengan gambar atau desain yang diminta dengan ketelitian yang sangat tinggi dan kualitas tidak kalah saing.</p>
                        <div class="float-right">
                            <button class="btn btn-primary font-weight-light px-4">Pilih</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 px-5 my-auto">
                <div class="card">
                    <div class="card-body bg-light rounded-lg px-5">
                        <h4 class="font-weight-bold text-center">Pembuatan Desain Teknik</h4>
                        <hr width="50%" color="black">
                        <p class="small">MM Side menyediakan arsitek handal untuk melayani pembuatan desain teknik sesuai selera dan keinginan, mulai dari desain furniture, rumah 2D dan 3D, pemetaan tanah, dll.</p>
                        <div class="float-right">
                            <button class="btn btn-primary font-weight-light px-4">Pilih</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container pt-5">
        <h2 class="text-center mb-5">Galeri</h2>
        <div class="">
            <div class="card">
                <div class="card-body py-2">
                    <a class="" href="#" id="kategori" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Kategori <span class="fa fa-chevron-down fa-fw"></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="kategori">
                        <a class="dropdown-item" href="#">Profil</a>
                        <a class="dropdown-item" href="#">Keluar</a>
                    </div>
                    <a class="float-right" href="#">
                        Lihat Lebih <span class="fa fa-angle-double-right"></span>
                    </a>
                </div>
            </div>
            a
        </div>
    </div>
</section>
@endsection
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>@yield('title')</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugin/fontawesome-free/css/all.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('plugin/overlayScrollbars/css/OverlayScrollbars.min.cs') }}s">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    </head>
    <body>

    <section>
        <div class="container">
            <nav class="navbar navbar-light navbar-expand-lg bg-transparent justify-content-between">
                <a class="navbar-brand" href="#">
                    <img src="/docs/4.0/assets/brand/bootstrap-solid.svg" width="30" height="30" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarDropdown" aria-controls="navbarDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarDropdown">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item my-2 active">
                            <a class="nav-link" href="#">Beranda</a>
                        </li>
                        <li class="nav-item my-2">
                            <a class="nav-link" href="#">
                                <span class="fa fa-envelope fa-fw"></span>
                            </a>
                        </li>
                        <li class="nav-item my-2">
                            <a class="nav-link" href="#">
                                <span class="fa fa-shopping-cart fa-fw"></span>
                            </a>
                        </li>
                        <li class="nav-item my-2">
                            <a class="nav-link disabled" href="#">|</a>
                        </li>
                        @if (isset(Auth::user()->name))
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="userLogin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-user fa-fw"></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="userLogin">
                                <a class="dropdown-item" href="#">Profil</a>
                                <a class="dropdown-item" href="#">Keluar</a>
                            </div>
                        </li>
                        @else
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <button class="btn btn-primary">Masuk</button>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <button class="btn btn-secondary bg-transparent text-primary">Daftar</button>
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </nav>
        </div>
    </section>
    
    @yield('content')
    <div style="margin-top:100px"></div>
    <footer class="page-footer bg-secondary">
        <svg id="" preserveAspectRatio="xMidYMax meet" class="svg-separator sep1" viewBox="0 0 1600 200" data-height="100" style="transform: translateY(-50%) translateY(2px);">
            <path class="" style="opacity: 1;fill: #6c757d;" d="M1040,56c0.5,0,1,0,1.6,0c-16.6-8.9-36.4-15.7-66.4-15.7c-56,0-76.8,23.7-106.9,41C881.1,89.3,895.6,96,920,96
                C979.5,96,980,56,1040,56z"></path>
            <path class="" style="opacity: 0.7;fill: #6c757d;" d="M1699.8,96l0,10H1946l-0.3-6.9c0,0,0,0-88,0s-88.6-58.8-176.5-58.8c-51.4,0-73,20.1-99.6,36.8
                c14.5,9.6,29.6,18.9,58.4,18.9C1699.8,96,1699.8,96,1699.8,96z"></path>
            <path class="" style="opacity: 0.7;fill: #6c757d;" d="M1400,96c19.5,0,32.7-4.3,43.7-10c-35.2-17.3-54.1-45.7-115.5-45.7c-32.3,0-52.8,7.9-70.2,17.8
                c6.4-1.3,13.6-2.1,22-2.1C1340.1,56,1340.3,96,1400,96z"></path>
            <path class="" style="opacity: 0.7;fill: #6c757d;" d="M320,56c6.6,0,12.4,0.5,17.7,1.3c-17-9.6-37.3-17-68.5-17c-60.4,0-79.5,27.8-114,45.2
                c11.2,6,24.6,10.5,44.8,10.5C260,96,259.9,56,320,56z"></path>
            <path class="" style="opacity: 0.7;fill: #6c757d;" d="M680,96c23.7,0,38.1-6.3,50.5-13.9C699.6,64.8,679,40.3,622.2,40.3c-30,0-49.8,6.8-66.3,15.8
                c1.3,0,2.7-0.1,4.1-0.1C619.7,56,620.2,96,680,96z"></path>
            <path class="" style="opacity: 0.7;fill: #6c757d;" d="M-40,95.6c28.3,0,43.3-8.7,57.4-18C-9.6,60.8-31,40.2-83.2,40.2c-14.3,0-26.3,1.6-36.8,4.2V106h60V96L-40,95.6
                z"></path>
            <path class="" style="opacity: 0.5;fill: #6c757d;" d="M504,73.4c-2.6-0.8-5.7-1.4-9.6-1.4c-19.4,0-19.6,13-39,13c-19.4,0-19.5-13-39-13c-14,0-18,6.7-26.3,10.4
                C402.4,89.9,416.7,96,440,96C472.5,96,487.5,84.2,504,73.4z"></path>
            <path class="" style="opacity: 0.5;fill: #6c757d;" d="M1205.4,85c-0.2,0-0.4,0-0.6,0c-19.5,0-19.5-13-39-13s-19.4,12.9-39,12.9c0,0-5.9,0-12.3,0.1
                c11.4,6.3,24.9,11,45.5,11C1180.6,96,1194.1,91.2,1205.4,85z"></path>
            <path class="" style="opacity: 0.5;fill: #6c757d;" d="M1447.4,83.9c-2.4,0.7-5.2,1.1-8.6,1.1c-19.3,0-19.6-13-39-13s-19.6,13-39,13c-3,0-5.5-0.3-7.7-0.8
                c11.6,6.6,25.4,11.8,46.9,11.8C1421.8,96,1435.7,90.7,1447.4,83.9z"></path>
            <path class="" style="opacity: 0,5;fill: #6c757d;" d="M985.8,72c-17.6,0.8-18.3,13-37,13c-19.4,0-19.5-13-39-13c-18.2,0-19.6,11.4-35.5,12.8
                c11.4,6.3,25,11.2,45.7,11.2C953.7,96,968.5,83.2,985.8,72z"></path>
            <path class="" style="opacity: 0.5;fill: #6c757d;" d="M743.8,73.5c-10.3,3.4-13.6,11.5-29,11.5c-19.4,0-19.5-13-39-13s-19.5,13-39,13c-0.9,0-1.7,0-2.5-0.1
                c11.4,6.3,25,11.1,45.7,11.1C712.4,96,727.3,84.2,743.8,73.5z"></path>
            <path class="" style="opacity: 0.5;fill: #6c757d;" d="M265.5,72.3c-1.5-0.2-3.2-0.3-5.1-0.3c-19.4,0-19.6,13-39,13c-19.4,0-19.6-13-39-13
                c-15.9,0-18.9,8.7-30.1,11.9C164.1,90.6,178,96,200,96C233.7,96,248.4,83.4,265.5,72.3z"></path>
            <path class="" style="opacity: 0.5;fill: #6c757d;" d="M1692.3,96V85c0,0,0,0-19.5,0s-19.6-13-39-13s-19.6,13-39,13c-0.1,0-0.2,0-0.4,0c11.4,6.2,24.9,11,45.6,11
                C1669.9,96,1684.8,96,1692.3,96z"></path>
            <path class="" style="opacity: 0.5;fill: #6c757d;"
                d="M25.5,72C6,72,6.1,84.9-13.5,84.9L-20,85v8.9C0.7,90.1,12.6,80.6,25.9,72C25.8,72,25.7,72,25.5,72z"></path>
            <path class="" style="opacity:1;fill: #6c757d;" d="M-40,95.6C20.3,95.6,20.1,56,80,56s60,40,120,40s59.9-40,120-40s60.3,40,120,40s60.3-40,120-40
                s60.2,40,120,40s60.1-40,120-40s60.5,40,120,40s60-40,120-40s60.4,40,120,40s59.9-40,120-40s60.3,40,120,40s60.2-40,120-40
                s60.2,40,120,40s59.8,0,59.8,0l0.2,143H-60V96L-40,95.6z"></path>
        </svg>
        <div class="container">
            <div class="row text-white card-body">
                <div class="col-lg-4">
                    <div class="mb-5">
                        <h3 class="footer-heading mb-4">MMSide.com</h3>
                        <p class="pr-4">Penjualan dan Penyedia Barang dan Jasa Layanan Properti Sebagai Inovasi dari Rakyat untuk Rakyat Melalui Gerai dan Website </p>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="row mb-5">
                        <h3 class="footer-heading mb-4">Alamat</h3>
                        <p class="pr-4">Jl. Balai Desa, Desa Karang Sari, Kecamatan Kembaran, Kabupaten Banyumas.</p>
                    </div>
                </div>

                <div class="col-lg-4 mb-5 mb-lg-0">
                    <h3 class="footer-heading mb-4">Narahubung</h3>
                    <div class="mb-5 pr-4">
                        <a href="https://wa.wizard.id/d1a2b2" class="mr-3 text-light"><span class="fa fa-phone-alt  fa-fw"></span>
                        <span class="d-md-inline-block ml-2">+62 895 4219 33116 (Andis)</span></a><br><br>
                        <a href="" class="mr-3 text-light"><span class="fa fa-envelope fa-fw"></span>
                        <span class="d-md-inline-block ml-2">mm.business234@gmail.com</span></a>
                    </div>
                </div>

            </div>
        </div>
        <div class="container-fluid">
            <div class="row pt-5 mt-5 text-center text-secondary">
                <div class="col-md-12 pt-4 bg-dark">
                <p>

                    Copyright &copy;<script>
                    document.write(new Date().getFullYear());
                    </script> <a href="index.html">MMSide</a>

                </p>
                </div>
            </div>
        </div>
    </footer>

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('plugin/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('plugin/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugin/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('assets/js/adminlte.js') }}"></script>

    <!-- OPTIONAL SCRIPTS -->
    <script src="{{ asset('assets/js/demo.js') }}"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="{{ asset('plugin/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('plugin/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugin/jquery-mapael/jquery.mapael.min.js') }}"></script>
    <script src="{{ asset('plugin/jquery-mapael/maps/usa_states.min.js') }}"></script>
    <!-- ChartJS -->
    <script src="{{ asset('plugin/chart.js/Chart.min.js') }}"></script>

    <!-- PAGE SCRIPTS -->
    <script src="{{ asset('assets/js/pages/dashboard2.js') }}"></script>
    @yield('jScript')
</body>
</html>
  